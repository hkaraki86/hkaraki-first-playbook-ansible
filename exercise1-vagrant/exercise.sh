ansible all -i vagrant.py -m selinux -a "state=disabled" -u root --become
ansible all -i vagrant.py -m shell -a "useradd karh002" -u root --become
ansible host02 -i vagrant.py -m yum -a "name=java-1.8.0-openjdk" -u root --become
ansible host03 -i vagrant.py -m yum -a "name=java-1.8.0-openjdk" -u root --become
ansible all -i vagrant.py -m yum -a "name=java-1.8.0-openjdk state=present" -u root --become
ansible all -i vagrant.py -m shell -a "shutdown -r now" -u root --become
